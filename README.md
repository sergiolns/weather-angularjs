# Weather-AngularJS
![](./images/logo_transparent.png)

####Dependencies

#####Install dependencies
>npm install


####How to fix "ReferenceError: primordials is not defined" error

#####Here's what you need to do:
In the same directory where you have package.json create an npm-shrinkwrap.json file with the following contents:

```
{
  "dependencies": {
    "graceful-fs": {
        "version": "4.2.2"
     }
  }
}
```

####Running project on development

#####Open your Terminal/Shell and type:

>grunt dev

After the command your application should start right in your default browser at localhost:4000



####Running project on production

#####Open your Terminal/Shell and type:

>grunt build

The Gruntfile.js already have some tasks like: Concat, Uglify, Injector and template cache.


####Running Tests

The tests are written in Jasmine, which we run with the [Karma Test Runner][karma]. We provide a Karma configuration 
file pre-configured with some default options to run them.

    the configuration is found at karma.conf.js
    the unit tests are found on each module created named as moduleName-test.js.

####The easiest way to run the unit tests is to use the supplied npm script on package.json file:

>npm test

This script will start the Karma test runner to execute the unit tests.
