(function () {
	'use strict';

	/**
	* @ngdoc function
	* @name app.service:homeService
	* @description
	* # homeService
	* Service of the app
	*/


	angular.module('weather')
		.factory('homeService', homeService);

	homeService.$inject = ['$http','$q'];

	function homeService($http, $q) {

			let weatherCity = {};

		function __getWeather(searcher, CITY_NAME) {
			let d = $q.defer();
				d.resolve($.getJSON(searcher.apiUrl + CITY_NAME + searcher.apiUrlSecond + searcher.APP_ID)
					.then(function(res) {
						return res;
					}, function(res){
						return $q.reject({'erro':'Nao Encontrado'});
					}));
			return d.promise;
			}
			weatherCity.getWeather = __getWeather;
			return weatherCity;
	}

})();
