(function () {
	'use strict';

	/**
	 * @ngdoc function
	 * @name app.test:homeFilter
	 * @description
	 * # capitalize
	 * Test of Filter
	 */

	describe('homeFilter, capitalize', function() {

		let filtroTeste;

		beforeEach(angular.mock.module('weather'));
		beforeEach(inject(function (_$filter_) {
			filtroTeste = _$filter_('capitalize');
		}));

		it('Should convert first char of string in uppercase', function () {
			expect(filtroTeste('true')).toEqual('True');
			expect(filtroTeste('clear sky')).toEqual('Clear sky');
		});

		it('DON`T convert first char of string in uppercase', function () {
			expect(filtroTeste('1true')).not.toEqual('1True');
		});

		it('DON`T convert first char to uppercase if not string', function () {
			expect(filtroTeste(1234)).toEqual(1234);
		});

	});
})();
