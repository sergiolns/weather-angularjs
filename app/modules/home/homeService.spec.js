(function () {
	'use strict';

	/**
	 * @ngdoc function
	 * @name app.test:homeService
	 * @description
	 * # homeService
	 * Test of Service
	 */

describe('Teste weather API', function () {

	let weatherTeste, $q, $httpBackend, env;

	let RESPONSE_SUCCESS = {
		"coord":{"lon":-8.69,"lat":41.18},
		"weather":[{"id":800,"main":"Clear","description":"clear sky","icon":"01d"}],
		"base":"stations",
		"main":{"temp":17.26,"feels_like":17.2,"temp_min":15,"temp_max":19.44,"pressure":1027,"humidity":77},
		"visibility":10000,
		"wind":{"speed":1.5,"deg":290},
		"clouds":{"all":0},"dt":1582554212,
		"sys":{"type":1,"id":6900,"country":"PT","sunrise":1582528676,"sunset":1582568313},
		"timezone":0,"id":2737824,
		"name":"Matosinhos Municipality",
		"cod":200
	};

	beforeEach(angular.mock.module('weather'));

	beforeEach(inject(function(homeService, __env) {
		weatherTeste = homeService;
		env = __env;
	}));

	it('Service (homeService) exist?', function() {
		expect(weatherTeste).toBeDefined();
	});

	describe('Method getWeather exist?', function () {

		let resultado;

		beforeEach(function() {
			resultado = {};

			spyOn(weatherTeste, "getWeather").and.returnValue(RESPONSE_SUCCESS);
		});

		it('Return a valid response if the name of the city is right', function() {

			const CITY_NAME = 'Matosinhos';

			expect(weatherTeste.getWeather).not.toHaveBeenCalled();
			expect(resultado).toEqual({});

			let resp = weatherTeste.getWeather(env, CITY_NAME);

			expect(weatherTeste.getWeather).toHaveBeenCalled();
			expect(weatherTeste.getWeather).toHaveBeenCalledWith(env, CITY_NAME);
			expect(resp.cod).toEqual(200);
			expect(resp.name).toEqual('Matosinhos Municipality');
			expect(resp.main.temp).toEqual(17.26);
		});
	});

});

})();
