'use strict';

	/**
	* @ngdoc function
	* @name app.route:HomeRoute
	* @description
	* # HomeRoute
	* Route of the app
	*/

angular.module('weather')
	.config(['$stateProvider', function ($stateProvider, $locationProvider) {
		$stateProvider

			.state('home', {
				url: '/',
				templateUrl: 'app/modules/home/home.html',
				controller: 'HomeCtrl',
				controllerAs: 'vm'
			});
		// $locationProvider.hashPrefix('');
		// $locationProvider.html5Mode(true);

	}]);
