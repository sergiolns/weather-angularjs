(function () {
	'use strict';

	/**
	 * @ngdoc function
	 * @name app.test:homeRouter
	 * @description
	 * # capitalize
	 * Test of Filter
	 */

describe('homeRoute', function() {
	let $location;

	beforeEach(function () {
		module('weather');
	});

	beforeEach(inject(function (_$location_) {
		$location = _$location_;
	}));

	it('Should match the path Module name', function () {
		$location.path('/home');
		expect($location.path()).toBe('/home');
	});
});

})();
