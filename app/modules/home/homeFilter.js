(function() {
	'use strict';

	/**
	 * @ngdoc function
	 * @name app.filter:homeFilter
	 * @description
	 * # capitalize
	 * Filter to capitalize words
	 */

	angular.module('weather')
		.filter('capitalize', capitalize);

			function capitalize () {
				return function(input) {
					return (angular.isString(input) && input.length > 0) ?
						input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() :
						input;
					};
			}
})();
