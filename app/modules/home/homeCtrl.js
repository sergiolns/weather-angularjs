(function () {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:HomeCtrl
	* @description
	* # HomeCtrl
	* Controller of the app
	*/

	angular
		.module('weather')
		.controller('HomeCtrl', Home);

	Home.$inject = ['homeService', '$scope', '__env'];


	function Home(homeService, $scope, __env) {

		$scope.myFunc = function(city) {

			let vm = this;

			const ELEMENT_SEARCHED_CITY = document.querySelector('#city').value.trim();
			const ELEMENT_LOADING_TEXT = document.querySelector('#load');
			const ELEMENT_WEATHER_BOX = document.querySelector('#weather');

			if (ELEMENT_SEARCHED_CITY.length === 0) {
				return alert('Please enter a city name');
			}

			ELEMENT_LOADING_TEXT.style.display = 'block';

			homeService.getWeather(__env, city)
				.then(function (dataWeather) {
					$scope.erro = null;
					vm.cityName = dataWeather.name;
					vm.weatherDescription = dataWeather.weather[0].description;
					vm.weatherTemperature = dataWeather.main.temp;
					ELEMENT_WEATHER_BOX.style.display = 'block';
				}, function(res){
					$scope.erro = "Houve um erro na sua pesquisa.";
				});

				ELEMENT_LOADING_TEXT.style.display = 'none';

		};

	}

})();
