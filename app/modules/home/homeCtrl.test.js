(function () {
	'use strict';

	/**
	* @ngdoc function
	* @name app.test:homeTest
	* @description
	* # homeTest
	* Test of the app
	*/

	describe('homeCtrl', function () {
		let controller = null, $scope = null, $location;

		beforeEach(function () {
			module('weather');
		});

		beforeEach(inject(function ($controller, $rootScope) {
			$scope = $rootScope.$new();
		}));

		it('Should HomeCtrl must be defined', function () {
			expect(controller).toBeDefined();
		});

	});
})();
