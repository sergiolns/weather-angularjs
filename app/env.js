(function (window) {
	window.__env = window.__env || {};

	// API url
	window.__env.apiUrl = 'http://api.openweathermap.org/data/2.5/weather?q=';
	window.__env.apiUrlSecond = '&units=metric&appid=';

	// API ID
	window.__env.APP_ID = '8e9b648df7ffadd06a00a934f14f676a';

	// Base url
	window.__env.baseUrl = '/';

	// Whether or not to enable debug mode
	// Setting this to false will disable console output
	window.__env.enableDebug = true;
}(this));
