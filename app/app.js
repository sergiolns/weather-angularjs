(function() {
	'use strict';

	/**
	* @ngdoc index
	* @name app
	* @description
	* # app
	*
	* Main module of the application.
	*/

	/**************************************************************************
	 * Set environment values
	 *************************************************************************/

		// Default environment variables
	var __env = {};

	// Import variables if present
	if(window){
		Object.assign(__env, window.__env);
	}


	/**************************************************************************
	 * Define Angular application
	 *************************************************************************/
	// Define AngularJS application
	var ngModule = angular.module('weather', [
		'ngResource',
		'ngAria',
		'ui.bootstrap',

		'ngCookies',
		'ngAnimate',
		'ngTouch',
		'ngSanitize',
		'ui.router',
		'home',
	]);


	/**************************************************************************
	 * Make environment available in Angular
	 *************************************************************************/
	// Register environment in AngularJS as constant
	ngModule.constant('__env', __env);


	/**************************************************************************
	 * Configure logging
	 *************************************************************************/
	function disableLogging($logProvider, __env){
		$logProvider.debugEnabled(__env.enableDebug);
	}

	// Inject dependencies
	disableLogging.$inject = ['$logProvider', '__env'];

	ngModule.config(disableLogging);


})();
